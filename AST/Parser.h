#ifndef PARSER_H
#define PARSER_H

#include "Lexer.h"
#include "Ast.h"

class Parser {
public:
	Parser(Lexer &lexer): lexer (lexer) { }
	UPStatementList parseCode();
    Token getToken() { return currToken; }
    
private:
	Token currToken;
	Lexer &lexer;

    void consumeToken();
	UPStatementList input();
    void headerDefinition();
    UPStatement definition();
	UPStatement functionDefinition(std::string type, std::string id);
    UPStatementList compoundStatement();
    UPStatement declaration(std::string type, std::string id);
    UPStatement statement();
    UPExpr expression();
    UPExpr logicalTerm();
    UPExpr bitwiseTerm();
    UPExpr relationalTerm();
    UPExpr relationalGLETerm();
    UPExpr bitwiseShiftTerm();
    UPExpr addSubTerm();
    UPExpr mulDivModTerm();
    UPExpr factor();
};

#endif