#ifndef TOKEN_H
#define TOKEN_H

enum class Token: unsigned int {
    /* Key Words */
    KwInt,
    KwVoid,
    KwChar,
    KwIf,
    KwElse,
    KwDo,
    KwWhile,
    KwFor,
    KwBreak,
    KwContinue,
    KwReturn,
    KwBool,
    KwTrue,
    KwFalse,
	
    /* Operators */
    OpAdd,
    OpSub,
    OpMul,
    OpDiv,
    OpMod,
    OpIncrement,
    OpDecrement,
    OpEqual,
    OpNotEqual,
    OpGreater,
    OpLess,
    OpGreaterEqual,
    OpLessEqual,
    OpAnd,
    OpOr,
    OpLogicalNot,
    OpBitwiseAnd,
    OpBitwiseOr,
    OpBitwiseXor,
    OpBitwiseNot,
	OpShiftRight,
    OpShiftLeft,
	OpAssign,

	/* Special Symbols */
    OpenPar,
    ClosePar,
    OpenCurlyBraces,
    CloseCurlyBraces,
    OpenBrackets,
    CloseBrackets,
    Comma,
    Semicolon,

    /* Others */
    StrLiteral,
    CharLiteral,
	Decimal,
	Hexadecimal,
	Binary,
    Octal,
	Identifier,
    Include,
    Eof,

    Undefined
};

static const char *kw[] = {
	"int",
	"void",
	"char",
    "bool",
	"if",
	"else",
	"for",
	"do",
	"return",
	"while",
    "break",
	"continue",
    "true",
    "false",
	0
};

static Token kwTk[] = {
    Token::KwInt,
    Token::KwVoid,
    Token::KwChar,
    Token::KwBool,
    Token::KwIf,
    Token::KwElse,
    Token::KwFor,
    Token::KwDo,
    Token::KwReturn,
    Token::KwWhile,
    Token::KwBreak,
    Token::KwContinue,
    Token::KwTrue,
    Token::KwFalse
};

#endif