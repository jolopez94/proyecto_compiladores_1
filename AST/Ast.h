#ifndef AST_H
#define AST_H

#include <memory>
#include <list>
#include <sstream>
#include <string>
#include <unordered_map>
#include <iostream>

#define DEFINE_BINARY_EXPR(name, oper, prec, op)            \
    class name##Expr: public BinaryExpr {               \
    public:                                             \
        name##Expr(UPExpr expr1, UPExpr expr2):         \
            BinaryExpr(std::move(expr1), std::move(expr2)) {}     \
                                                        \
        std::string getOper() override { return oper; }      \
        int getPrecedence() override { return prec; }         \
        int eval(GlobalContext &ctx) override { return this->expr1->eval(ctx) op this->expr2->eval(ctx); }\
    }

class ASTNode;
class Expr;
class Statement;
class FunctionDeclStatement;

using UPASTNode = std::unique_ptr<ASTNode>;
using UPASTNodeList = std::list<UPASTNode>;
using UPExpr = std::unique_ptr<Expr>;
using UPExprList = std::list<UPExpr>;
using UPStatement = std::unique_ptr<Statement>;
using UPStatementList = std::list<UPStatement>;
using StringList = std::list<std::string>;

struct value {
    std::string type;
    int value;
};

struct func {
    std::string type, id;
    StringList idList;
    UPStatementList stmts;
};

class GlobalContext {
public:
    GlobalContext() { this->breakFlag = false; this->continueFlag = false; }

    std::unordered_map<std::string, struct value> vars;
    std::unordered_map<std::string, struct func> funcs;
    bool breakFlag, continueFlag, returnFlag;
    int returnValue;
};

class ASTNode {
public:
    virtual ~ASTNode() {}
    virtual std::string toString() = 0;
};

class Expr: public ASTNode {
public:
    virtual int getPrecedence() = 0;
    virtual int eval(GlobalContext& ctx) = 0;
};

class BinaryExpr: public Expr {
public:
    BinaryExpr(UPExpr expr1, UPExpr expr2):
        expr1(std::move(expr1)), expr2(std::move(expr2)) {}

    UPExpr expr1, expr2;

    std::string toString() override;
    virtual std::string getOper() = 0;
};

DEFINE_BINARY_EXPR(And, "&&", 0, &&);
DEFINE_BINARY_EXPR(Or, "||", 0, ||);
DEFINE_BINARY_EXPR(BitwiseOr, "|", 1, |);
DEFINE_BINARY_EXPR(BitwiseXor, "^", 1, ^);
DEFINE_BINARY_EXPR(BitwiseAnd, "&", 1, &);
DEFINE_BINARY_EXPR(Equal, "==", 2, ==);
DEFINE_BINARY_EXPR(NotEqual, "!=", 2, !=);
DEFINE_BINARY_EXPR(Greater, ">", 3, >);
DEFINE_BINARY_EXPR(GreaterEqual, ">=", 3, >=);
DEFINE_BINARY_EXPR(Less, "<", 3, <);
DEFINE_BINARY_EXPR(LessEqual, "<=", 3, <=);
DEFINE_BINARY_EXPR(ShiftLeft, "<<", 4, <<);
DEFINE_BINARY_EXPR(ShiftRight, ">>", 4, >>);
DEFINE_BINARY_EXPR(Add, "+", 5, +);
DEFINE_BINARY_EXPR(Sub, "-", 5, -);
DEFINE_BINARY_EXPR(Mul, "*", 6, *);
DEFINE_BINARY_EXPR(Mod, "%", 6, %);

class DivExpr: public BinaryExpr {            
public:                                            
    DivExpr(UPExpr expr1, UPExpr expr2):
        BinaryExpr(std::move(expr1), std::move(expr2)) {}

        std::string getOper() override { return "/"; }
        int getPrecedence() override { return 6; }
        int eval(GlobalContext &ctx) override {
            if (this->expr2->eval(ctx) == 0) {
                throw "Semantic: Warning division by 0";
            }
            return this->expr1->eval(ctx) / this->expr2->eval(ctx); 
        }
};

class BoolExpr: public Expr {
public:
    BoolExpr(int value):
        value(value) {};

    int value;

    std::string toString() override { return (value > 0) ? "true" : "false"; }
    int getPrecedence() override { return 7; }
    int eval(GlobalContext &ctx) override { return value; }
};

class NumExpr: public Expr {
public:
    NumExpr(int value):
        value(value) {};
    
    int value;

    std::string toString() override { return std::to_string(value); }
    int getPrecedence() override { return 7; }
    int eval(GlobalContext &ctx) override { return value; }
};

class IdExpr: public Expr {
public:
    IdExpr(std::string name):
        name(name) {};

    std::string name;

    std::string toString() override { return name; }
    int getPrecedence() override { return 7; }
    int eval(GlobalContext &ctx) override;
};

class CharExpr: public Expr {
public:
    CharExpr(char value):
        value(value) {};

    char value;

    std::string toString() override { return "'" + std::string(1, value) + "'"; }
    int getPrecedence() override { return 7; }
    int eval(GlobalContext &ctx) override { return value; }
};

class FunctionExpr: public Expr {
public:
    FunctionExpr(std::string id, UPExprList exprList):
        id(id), exprList(std::move(exprList)) {};

    std::string id;
    UPExprList exprList;
    GlobalContext localCtx;
    std::string toString() override;
    int getPrecedence() override { return 7; }
    int eval(GlobalContext &ctx) override;
};

class Statement: public ASTNode {
public:
    virtual void execute(GlobalContext &ctx) = 0;
};

class DeclareStatement: public Statement {
public:
    DeclareStatement(std::string type, StringList idList, UPExprList exprList, int lineNo):
        type(type), idList(idList), exprList(std::move(exprList)), lineNo(lineNo) {};

    std::string type;
    StringList idList;
    UPExprList exprList;
    int lineNo;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class FunctionDeclStatement: public Statement {
public:
    FunctionDeclStatement(std::string type, std::string id, StringList typeList, StringList idList, UPStatementList stmts, int lineNo):
        type(type), id(id), typeList(typeList), idList(idList), stmts(std::move(stmts)), lineNo(lineNo) {};
    
        std::string type, id;
        StringList typeList, idList;
        UPStatementList stmts;
        int lineNo;

        std::string toString() override;
        void execute(GlobalContext &ctx) override;
};

class AssignStatement: public Statement {
public:
    AssignStatement(std::string id, UPExpr expr, int lineNo):
        id(id), expr(std::move(expr)), lineNo(lineNo) { indxExpr = nullptr; }
    
    AssignStatement(std::string id, UPExpr expr, UPExpr indxExpr):
        id(id), expr(std::move(expr)), indxExpr(std::move(indxExpr)) {}

    std::string id;
    UPExpr expr, indxExpr;
    int lineNo;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class IfStatement: public Statement {
public:
    IfStatement(UPExpr cond, UPStatementList stmts1, UPStatementList stmts2):
        cond(std::move(cond)), stmts1(std::move(stmts1)), stmts2(std::move(stmts2)) {};
    
    UPExpr cond;
    UPStatementList stmts1, stmts2;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class ForStatement: public Statement {
public:
    ForStatement(std::string id, std::string incrId, UPExpr init, UPExpr cond, UPExpr incr, UPStatementList stmts, int lineNo):
        id(id), incrId(incrId), init(std::move(init)), cond(std::move(cond)), incr(std::move(incr)), stmts(std::move(stmts)), lineNo(lineNo) {}
    
    std::string id, incrId;
    UPExpr init, cond, incr;
    UPStatementList stmts;
    int lineNo;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class WhileStatement: public Statement {
public:
    WhileStatement(UPExpr cond, UPStatementList stmts):
        cond(std::move(cond)), stmts(std::move(stmts)) {}

    UPExpr cond;
    UPStatementList stmts;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class BreakStatement: public Statement {
public:
    std::string toString() override { return "break;"; }
    void execute(GlobalContext &ctx) override { ctx.breakFlag = true; }
};

class ContinueStatement: public Statement {
public:
    std::string toString() override { return "continue;"; }
    void execute(GlobalContext &ctx) override { ctx.continueFlag = true; }
};

class ReturnStatement: public Statement {
public:
    ReturnStatement() { this->expr = nullptr; }
    ReturnStatement(UPExpr expr):
        expr(std::move(expr)) {};
    
    UPExpr expr;

    std::string toString() override;
    void execute(GlobalContext &ctx) override;
};

class FunctionStatement: public Statement {
public:
    FunctionStatement(std::string id, UPExprList exprList):
        id(id), exprList(std::move(exprList)) {};

    std::string id;
    UPExprList exprList;

    std::string toString() override;
    void execute(GlobalContext &ctx) override { std::cout << "An exe should be in here" << std::endl; }
};

#endif