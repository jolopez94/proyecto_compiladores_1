#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>
#include "Token.h"

#define UNKNOWN_TK  0
#define UNCLOSED_TK 1
#define INT_CONSTNT 2
#define EXPECTED_TK 3

/* Declared Functions */

void error (int errorNo, int lineNo, std::string s1, std::string s2);
bool isStatement (Token tk);
bool isExpression (Token tk);

/* Defined Functions */

static inline bool isoctal (char ch) {
    return ch >= '0' && ch <= '7';
}

static inline bool isTypeSpecifier (Token tk) {
    return static_cast<unsigned int>(tk) <= 3 || tk == Token::KwBool;
}

#endif
