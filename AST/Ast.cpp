#include "Ast.h"

std::string BinaryExpr::toString() {
    std::string s1 = expr1->toString();
    std::string s2 = expr2->toString();

    if (getPrecedence() > expr1->getPrecedence()) {
        s1 = "(" + s1 + ")";
    }
    if (getPrecedence() > expr2->getPrecedence()) {
        s2 = "(" + s2 + ")";
    }

    return s1 + getOper() + s2;
}

std::string FunctionExpr::toString() {
    std::ostringstream ss;
    int count = 1, size = exprList.size();

    ss << this->id << "(";

    for (const auto &expr: this->exprList) {
        if (count < size) {
            ss << expr->toString() << ", ";
            count++;
        } else {
            ss << expr->toString();
        }
    }

    ss << ")";

    return ss.str();
}

int FunctionExpr::eval(GlobalContext &ctx) {
    int aux = 0;
    auto search = std::move(ctx.funcs.find(this->id));
    struct value val;

    if (search == ctx.funcs.end()) {
        throw "Semantic: Function " + this->id + " is not declared";
    }

    struct func currFunc = std::move(ctx.funcs.at(this->id));

    if (currFunc.stmts.empty()) {
        throw "Semantic: Function " + this->id + " is not defined";
    }

    if (currFunc.type == "void") {
        throw "Semantic: Function " + this->id + " return type is void";
    }

    if (currFunc.idList.size() != this->exprList.size()) {
        throw "Semantic: Function " + this->id + " has " + std::to_string(currFunc.idList.size()) + " parameters defined, function is receiving " + std::to_string(this->exprList.size()) + " parameters";
    }



    for (auto& it: ctx.vars) {
        val.type = it.second.type;
        val.value = it.second.value;
        localCtx.vars.emplace(it.first, val);
    }

    val.type = "int";
    val.value = 0;

    for (auto const &id: currFunc.idList) {
        auto it = std::next(exprList.begin(), aux);
        val.value = (*it)->eval(ctx);
        localCtx.vars.emplace(id, val);
        aux++;
    }
    
    for (auto const &stmt: currFunc.stmts) {
        stmt->execute(localCtx);

        if (localCtx.returnFlag) {
            aux = localCtx.returnValue;
            break;
        }
    }

    if (!localCtx.returnFlag) {
        throw "Semantic: Function " + this->id + " has no return value ";
    }
    
    localCtx.returnFlag = true;
    return aux;
}

std::string DeclareStatement::toString() {
    std::ostringstream ss;
    int count = 0, size = (idList.size() - 1);

    ss << this->type << " ";
    
    for (const auto &id: idList) {
        if (count < size) {
            ss << id;

            auto it = std::next(exprList.begin(), count);
            if (*it != nullptr) {
                ss << " = " << (*it)->toString();
            }

            ss << ",";
            count++;

        } else {
            ss << id;

            auto it = std::next(exprList.begin(), count);
            if (*it != nullptr) {
                ss << "=" << (*it)->toString();
            }
        }
    }

    ss << ";";

    return ss.str();
}

void DeclareStatement::execute(GlobalContext &ctx) {
    std::string type = this->type;
    struct value newVal;
    int count = 0;

    if (type == "void") {
        throw "Line:" + std::to_string(this->lineNo) + ":Semantic: Type void is used to signify the absence of a type, unable to declare void variables";
    }

    for (const auto &id: idList) {
        auto search = ctx.vars.find(id);

        if (search != ctx.vars.end()) {
            throw "Line:" + std::to_string(this->lineNo) + ":Semantic: Variable " + id + " is already declared";
        } else {
            newVal.type = this->type;
            auto it = std::next(exprList.begin(), count);

            try {
                (*it)->eval(ctx);
            } catch (std::string msg) {
                throw "Line:" + std::to_string(lineNo) + ":" + msg;
            }

            newVal.value = (*it)->eval(ctx);
            ctx.vars.emplace(id, newVal);
        }
        count++;
    }
}

std::string FunctionDeclStatement::toString() {
    std::ostringstream ss;
    int count = 0, size = (typeList.size() - 1);

    ss << this->type << " " << this->id << "(";

    if (!typeList.empty()) {
        for (const auto &type: typeList) {
            if (count < size) {
                ss << type << " ";
                auto it = std::next(this->idList.begin(), count);
                ss << (*it) << ", ";
                count++;
            } else {
                ss << type << " ";
                auto it = std::next(this->idList.begin(), count);
                ss << (*it);
            }
        }
    }

    ss << ")";

    if (!stmts.empty()) {
        ss << "{";

        for (const auto &stmt: stmts) {
            ss << stmt->toString();
        }

        ss << "}";
    } else {
        ss << ";";
    }

    return ss.str();
}

void FunctionDeclStatement::execute(GlobalContext &ctx) {
    if (this->id == "main") {
        for (auto const &stmt: this->stmts) {
            stmt->execute(ctx);
        }
    } else {
        auto search = ctx.funcs.find(this->id);

        if (search != ctx.funcs.end()) {
            throw "Line:" + std::to_string(lineNo) + ":Semantic: Function with id " + this->id +" is already declared";
        }

        struct func newFunc;
        newFunc.id = this->id;
        newFunc.type = this->type;
        newFunc.idList = this->idList;
        newFunc.stmts = std::move(this->stmts);
        ctx.funcs.emplace(this->id, std::move(newFunc));
    }
}

std::string AssignStatement::toString() {
    std::ostringstream ss;
    ss << this->id;

    if (this->indxExpr != nullptr) {
        ss << "[" << this->indxExpr->toString() << "]";
    }
    ss << " = " << this->expr->toString() << ";";

    return ss.str();
}

void AssignStatement::execute(GlobalContext &ctx) {
    auto search = ctx.vars.find(this->id);

    if (search == ctx.vars.end()) {
        throw "Line:" + std::to_string(this->lineNo) + ":Semantic: Variable " + this->id + " is not declared";
    } else {
        ctx.vars.at(this->id).value = this->expr->eval(ctx);
    }
}

std::string IfStatement::toString() {
    std::ostringstream ss;

    ss << "if (" << this->cond->toString() << ") {";

    for (auto const &stmt: this->stmts1) {
        ss << stmt->toString();
    }

    ss << "}";

    if (!this->stmts2.empty()) {
        ss << "else{";

        for (auto const &stmt: this->stmts2) {
            ss << stmt->toString();
        }
        ss << "}";
    }
    
    return ss.str();
}

void IfStatement::execute(GlobalContext &ctx) {
    int value = this->cond->eval(ctx);

    if (value > 0) {
        for (auto const &stmt: this->stmts1) {
            stmt->execute(ctx);
        }
    } else {
        for (auto const &stmt: this->stmts2) {
            stmt->execute(ctx);
        }
    }
}

std::string ForStatement::toString() {
    std::ostringstream ss;
    
    ss << "for(" << this->id << "=" << this->init->toString() << ";" << this->cond->toString() << ";" << this->incrId << "=" << this->incr->toString() << "){";
    
    for (auto const &stmt: this->stmts) {
        ss << stmt->toString();
    }
    
    ss << "}";
    
    return ss.str();
}

void ForStatement::execute(GlobalContext &ctx) {
    auto search = ctx.vars.find(this->id);
    int cond = this->cond->eval(ctx);
    bool bFlag = false;

    ctx.breakFlag = false;
    ctx.continueFlag = false;

    if (search == ctx.vars.end()) {
        throw "Line:" + std::to_string(this->lineNo) + ":Semantic: Variable " + this->id + " is not declared";
    } else {
        ctx.vars.at(this->id).value = this->init->eval(ctx);
    }

    for (
        ctx.vars.at(this->id).value = this->init->eval(ctx); 
        this->cond->eval(ctx) > 0; 
        ctx.vars.at(this->incrId).value = this->incr->eval(ctx)
        ) {
        
        for (auto const &stmt: this->stmts) {
            stmt->execute(ctx);

            if (ctx.breakFlag == true) {
                ctx.breakFlag = false;
                bFlag = true;
                break;
            } else if (ctx.continueFlag == true) {
                ctx.continueFlag = false;
                break;
            }
        }

        if (bFlag)
            break;
    }
}

std::string WhileStatement::toString() {
    std::ostringstream ss;
    ss << "while(" << this->cond->toString() << "){";

    for (const auto &stmt: this->stmts) {
        ss << stmt->toString();
    }
    ss << "}";

    return ss.str();
}

void WhileStatement::execute(GlobalContext &ctx) {
    int cond = this->cond->eval(ctx);
    bool bFlag = false;

    ctx.breakFlag = false;
    ctx.continueFlag = false;

    while (cond > 0) {
        for (auto const &stmt: this->stmts) {
            stmt->execute(ctx);

            if (ctx.breakFlag == true) {
                ctx.breakFlag = false;
                bFlag = true;
                break;
            } else if (ctx.continueFlag == true) {
                ctx.continueFlag = false;
                break;
            }
        }

        if (bFlag)
            break;
        cond = this->cond->eval(ctx);
    }
}

std::string ReturnStatement::toString() {
    if (this->expr != nullptr)
        return "return " + this->expr->toString() + ";";
    else
        return "return;";
}

void ReturnStatement::execute(GlobalContext &ctx) {
    if (this->expr != nullptr) {
        ctx.returnValue = this->expr->eval(ctx);
        ctx.returnFlag = true;
    } else {
        ctx.returnValue = 0;
        ctx.returnFlag = true;
    }
}

std::string FunctionStatement::toString() {
    std::ostringstream ss;
    ss << this->id << "(";
    int count = 1, size = exprList.size();

    for (const auto &expr: this->exprList) {
        if (count < size) {
            ss << expr->toString() << ", ";
            count++;
        } else {
            ss << expr->toString();
        }
    }
    ss << ");";

    return ss.str();
}

int IdExpr::eval(GlobalContext &ctx) {
    auto search = ctx.vars.find(this->name);
    if (search == ctx.vars.end()) {
        throw "Semantic: Variable " + this->name + " was not found";
    }
    return ctx.vars.at(this->name).value;
}