#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <sstream>
#include <memory>
#include <cstring>
#include "doctest.h"
#include "Parser.h"

void consumeToken (Lexer &l, Token &tk) {
    try {
        tk = l.getNextToken();
    } catch (std::string msg) {
        std::cerr << msg << std::endl;
    }
}

const char *test1 = 
"int a = 10 + 13 - 15 * 13 / 10 % 14;\n"
"int b = 20 >> 30 << 20;\n"
"int c = 21 | 244 ^ 32 & 1;\n";

TEST_CASE("Int Expressions") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test1);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int a=10+13-15*13/10%14;int b=20>>30<<20;int c=21|244^32&1;");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test2 = 
"bool flag1 = true;\n"
"bool flag2 = false;\n"
"bool flag3 = a < b || a > c && b != 10 || b == 9;\n"
"bool flag4 = a >= b && b || c <= b;\n";

TEST_CASE("Bool Expressions") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test2);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();

    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "bool flag1=true;bool flag2=false;bool flag3=a<b||a>c&&b!=10||b==9;bool flag4=a>=b&&b||c<=b;");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test3 = 
"int a = 10 + 10;\n"
"int b = a;\n";

TEST_CASE("Id and Num Expressions") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test3);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int a=10+10;int b=a;");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test4 = 
"char a = 'a';\n"
"char b = 'b';\n"
"char c = 'c';";

TEST_CASE("Id and Num Expressions") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test4);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "char a='a';char b='b';char c='c';");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test5 = 
"int suma (int a, int b, int c);\n"
"int resta (int a, int b, int c);\n"
"void main() { suma (10, 20, 30); resta (10, 20, 30); }";

TEST_CASE("Function Expressions") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test5);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int suma(int a, int b, int c);int resta(int a, int b, int c);void main(){suma(10, 20, 30);resta(10, 20, 30);}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test6 = 
"int value1 = 1, value2 = 2, value3 = 3;\n"
"bool flag1 = true, flag2 = false, flag3 = true;\n"
"char c1 = 'a', c2 = 'b', c3 = 'c';";

TEST_CASE("Declare Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test6);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int value1 = 1,value2 = 2,value3=3;bool flag1 = true,flag2 = false,flag3=true;char c1 = 'a',c2 = 'b',c3='c';");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test7 = 
"int suma (int a, int b, int c) { return a + b + c; }\n"
"int resta (int a, int b, int c) { return a - b - c; }";

TEST_CASE("Function Declare Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test7);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int suma(int a, int b, int c){return a+b+c;}int resta(int a, int b, int c){return a-b-c;}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test8 =
"int main () { int num; char c_value; bool flag; num = 1; c_value = 'd'; flag = true; }";

TEST_CASE("Assign Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test8);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){int num;char c_value;bool flag;num = 1;c_value = 'd';flag = true;}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test9 =
"int main () { int num; bool flag; if (flag) { num = 1; } else { num = 2; } return num; }";

TEST_CASE("If and Else Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test9);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){int num;bool flag;if (flag) {num = 1;}else{num = 2;}return num;}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test10 =
"int main () { int num; bool flag; if (flag) { num = 1; } return num; }";

TEST_CASE("Loneley If Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test10);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){int num;bool flag;if (flag) {num = 1;}return num;}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test11 =
"int main () { bool flag = true; int num = 1; while (true) { num = num + 1; } }";

TEST_CASE("While Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test11);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){bool flag=true;int num=1;while(true){num = num+1;}}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test12 =
"int main () { bool flag = true; int num = 1; while (true) { num = num + 1; break; } }";

TEST_CASE("Break Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test12);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){bool flag=true;int num=1;while(true){num = num+1;break;}}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test13 =
"int main () { bool flag = true; int num = 1; while (true) { num = num + 1; continue; } }";

TEST_CASE("Break Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test13);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){bool flag=true;int num=1;while(true){num = num+1;continue;}}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test14 =
"int main () { bool flag = true; int num = 1; while (true) { num = num + 1; break; } return num; }";

TEST_CASE("Return Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test14);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int main(){bool flag=true;int num=1;while(true){num = num+1;break;}return num;}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test15 =
"void eval() { char c; }\n"
"int main () { eval(); }";

TEST_CASE("Function Statement") {
    std::istringstream in;
    std::ostringstream ss;
    bool parsed;

    in.str(test15);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "void eval(){char c;}int main(){eval();}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test16 = 
"int a = 10 + 13 * 2 - 1;\n"
"int b = 20 << 2;\n"
"int c = 20 >> 2;\n"
"int d = 1 | 1;\n"
"int e = 1 & 1;\n"
"int f = 1 ^ 1;";

TEST_CASE("Int Expressions Eval and Assign Execute") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test16);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 6);
    CHECK(ctx.vars.at("a").value == 35);
    CHECK(ctx.vars.at("b").value == 80);
    CHECK(ctx.vars.at("c").value == 5);
    CHECK(ctx.vars.at("d").value == 1);
    CHECK(ctx.vars.at("e").value == 1);
    CHECK(ctx.vars.at("f").value == 0);
    CHECK(ss.str() == "int a=10+13*2-1;int b=20<<2;int c=20>>2;int d=1|1;int e=1&1;int f=1^1;");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test17 = 
"int a = 10 + 13 * 2 - 1;\n"
"int b = 20 << 2;\n"
"int c = 20 >> 2;\n"
"int d = 1 | 1;\n"
"int e = 1 & 1;\n"
"int e = 1 ^ 1;";

TEST_CASE("Variable already exists error.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test17);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Variable already exists error.)" << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 5);
    CHECK(ctx.vars.at("a").value == 35);
    CHECK(ctx.vars.at("b").value == 80);
    CHECK(ctx.vars.at("c").value == 5);
    CHECK(ctx.vars.at("d").value == 1);
    CHECK(ctx.vars.at("e").value == 1);
    CHECK(ss.str() == "int a=10+13*2-1;int b=20<<2;int c=20>>2;int d=1|1;int e=1&1;int e=1^1;");
    CHECK(err.str() == "Line:6:Semantic: Variable e is already declared");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test18 = 
"int a = 10 + 13 * 2 - 1;\n"
"a = 20 << 2;";

TEST_CASE("Assign statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test18);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Assign statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 1);
    CHECK(ctx.vars.at("a").value == 80);
    CHECK(ss.str() == "int a=10+13*2-1;a = 20<<2;");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test19 = 
"int a = 10 + 13 * 2 - 1;\n"
"b = 20 << 2;\n";

TEST_CASE("Assign Statement Error") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test19);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Assign statement.)" << msg << std::endl;
            compiled = false;
            err << msg;
        }
    }
    
    CHECK(ctx.vars.size() == 1);
    CHECK(ctx.vars.at("a").value == 35);
    CHECK(ss.str() == "int a=10+13*2-1;b = 20<<2;");
    CHECK(err.str() == "Line:3:Semantic: Variable b is not declared");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test20 = 
"int a = 10;\n"
"bool flag = true;\n"
"int b = 5;\n"
"void main() { if (a > 9) { a = 1; } else { a = 2; }\n"
"if (b < a) { flag = false; } if (flag) { b = 10; } else { b = 1; } }";

TEST_CASE("If Statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test20);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (If statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 3);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 10);
    CHECK(ctx.vars.at("flag").value == 1);
    CHECK(ss.str() == "int a=10;bool flag=true;int b=5;void main(){if (a>9) {a = 1;}else{a = 2;}if (b<a) {flag = false;}if (flag) {b = 10;}else{b = 1;}}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test21 = 
"int c = 1;\n"
"void main() { while (c < 5) { c = c + 1; } }";

TEST_CASE("While Statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test21);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (If statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 1);
    CHECK(ctx.vars.at("c").value == 5);
    CHECK(ss.str() == "int c=1;void main(){while(c<5){c = c+1;}}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test22 = 
"int c = 1;\n"
"void main() { while (c < 5) { c = c + 1; if (c == 4) { break; } } }";

TEST_CASE("Break Statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test22);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Break statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 1);
    CHECK(ctx.vars.at("c").value == 4);
    CHECK(ss.str() == "int c=1;void main(){while(c<5){c = c+1;if (c==4) {break;}}}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test23 = 
"int c = 1;\n"
"int a = 1;\n"
"void main() { while (c < 5) { c = c + 1; if (c == 5) { continue; } a = a + 1; } }";

TEST_CASE("Continue Statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test23);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Continue statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.vars.at("c").value == 5);
    CHECK(ctx.vars.at("a").value == 4);
    CHECK(ss.str() == "int c=1;int a=1;void main(){while(c<5){c = c+1;if (c==5) {continue;}a = a+1;}}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test24 = 
"void c = 1;"
"void main() { while (c < 5) { c = c + 1; if (c == 5) { continue; } a = a + 1; } }";

TEST_CASE("Declaring void variables.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test24);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Declare void variables.)" << msg << std::endl;
            compiled = false;
            err << msg;
        }
    }

    CHECK(ctx.vars.size() == 0);
    CHECK(ss.str() == "void c=1;void main(){while(c<5){c = c+1;if (c==5) {continue;}a = a+1;}}");
    CHECK(err.str() == "Line:1:Semantic: Type void is used to signify the absence of a type, unable to declare void variablesSemantic: Variable c was not found");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

// const char *test25 = 
// "int c = 1/0;"
// "void main() { while (c < 5) { c = c + 1; if (c == 5) { continue; } a = a + 1; } }";

// TEST_CASE("Declaring void variables.") {
//     std::istringstream in;
//     std::ostringstream ss, err;
//     GlobalContext ctx;
//     bool parsed, compiled = true;

//     in.str(test25);
// 	Lexer l(in);
//     Parser p(l);

//     UPStatementList ast;

//     try {
//         ast = p.parseCode();
//         parsed = true;
//     } catch (std::string msg) {
//         parsed = false;
//         std::cerr << msg << std::endl;
//     }

//     Token tk = p.getToken();
    
//     for (auto const &node: ast) {
//         ss << node->toString();
//     }

//     for (auto const &node: ast) {
//         try {
//             node->execute(ctx);
//         } catch (std::string msg) {
//             std::cerr << "IN TEST (Declare void variables.)" << msg << std::endl;
//             compiled = false;
//             err << msg;
//         }
//     }

//     CHECK(ctx.vars.size() == 0);
//     CHECK(ss.str() == "void c=1;void main(){while(c<5){c = c+1;if (c==5) {continue;}a = a+1;}}");
//     CHECK(err.str() == "Line:1:Semantic: Type void is used to signify the absence of a type, unable to declare void variablesSemantic: Variable c was not found");
//     CHECK(parsed);
//     CHECK(!compiled);
//     CHECK(tk == Token::Eof);
// }

const char *test25 = 
"int c = 1;\n"
"int a = 0;\n"
"void main() { for(c = 0; c < 5; c = c + 1) { a = a + 1; } }";

TEST_CASE("For toString.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test25);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    CHECK(ss.str() == "int c=1;int a=0;void main(){for(c=0;c<5;c=c+1){a = a+1;}}");
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test26 = 
"int c = 0;\n"
"int a = 0;\n"
"void main() { for(c = 0; c < 5; c = c + 1) { a = a + 1; } }";

TEST_CASE("For Statement.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test26);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Continue statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.vars.at("c").value == 5);
    CHECK(ctx.vars.at("a").value == 5);
    CHECK(ss.str() == "int c=0;int a=0;void main(){for(c=0;c<5;c=c+1){a = a+1;}}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test27 = 
"int suma (int a, int b) { return a + b; }\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = a + b; }";

TEST_CASE("Declare Functions.") {
    std::istringstream in;
    std::ostringstream ss;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test27);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << "IN TEST (Continue statement.)" << msg << std::endl;
            compiled = false;
        }
    }
    
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 3);
    CHECK(ss.str() == "int suma(int a, int b){return a+b;}int a=1;int b=2;void main(){b = a+b;}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}

const char *test28 = 
"void hola(int a, int b) { return a + b; }\n"
"void hola(int a, int b) { return a - b; }\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = a + b; }";

TEST_CASE("Declare func errors Statement.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test28);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 5);
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 3);
    CHECK(ss.str() == "void hola(int a, int b){return a+b;}void hola(int a, int b){return a-b;}int a=1;int b=2;void main(){b = a+b;}");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test29 = 
"void suma(int a, int b) { return a + b; }\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = resta(a, b); }";

TEST_CASE("Call function errors.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test29);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 4);
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 2);
    CHECK(ss.str() == "void suma(int a, int b){return a+b;}int a=1;int b=2;void main(){b = resta(a, b);}");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test30 = 
"void suma(int a, int b);\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = suma(a, b); }";

TEST_CASE("Call function errors.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test30);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 4);
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 2);
    CHECK(ss.str() == "void suma(int a, int b);int a=1;int b=2;void main(){b = suma(a, b);}");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test31 = 
"void suma(int a, int b){ return a+b; }\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = suma(a, b); }";

TEST_CASE("Call function errors.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test31);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 4);
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 2);
    CHECK(ss.str() == "void suma(int a, int b){return a+b;}int a=1;int b=2;void main(){b = suma(a, b);}");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test32 = 
"int suma(int a, int b){ return a+b; }\n"
"int a = 1;\n"
"int b = 2;\n"
"void main() { b = suma(a); }";

TEST_CASE("Call function errors.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test32);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 4);
    CHECK(ctx.vars.size() == 2);
    CHECK(ctx.funcs.size() == 1);
    CHECK(ctx.vars.at("a").value == 1);
    CHECK(ctx.vars.at("b").value == 2);
    CHECK(ss.str() == "int suma(int a, int b){return a+b;}int a=1;int b=2;void main(){b = suma(a);}");
    CHECK(parsed);
    CHECK(!compiled);
    CHECK(tk == Token::Eof);
}

const char *test33 = 
"int suma(int lvalue, int rvalue){ return lvalue+rvalue; }\n"
"int resta(int lvalue, int rvalue){ return lvalue-rvalue; }\n"
"int multiplicacion(int lvalue, int rvalue){ return lvalue*rvalue; }\n"
"int division(int lvalue, int rvalue){ return lvalue/rvalue; }\n"
"int iffun(int lvalue, int rvalue){ if(lvalue > rvalue) {return lvalue;} else {return rvalue;} }\n"
"int a = 10;\n"
"int b = 5;\n"
"int res1 = 0;\n"
"int res2 = 0;\n"
"int res3 = 0;\n"
"int res4 = 0;\n"
"int res5 = 0;\n"
"void main() { res1 = suma(a, b); res2 = resta(a, b); res3 = multiplicacion(a, b); res4 = division(a, b); res5 = iffun(a, b); }";

TEST_CASE("Return functions.") {
    std::istringstream in;
    std::ostringstream ss, err;
    GlobalContext ctx;
    bool parsed, compiled = true;

    in.str(test33);
	Lexer l(in);
    Parser p(l);

    UPStatementList ast;

    try {
        ast = p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    for (auto const &node: ast) {
        ss << node->toString();
    }

    for (auto const &node: ast) {
        try {
            node->execute(ctx);
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
            err << msg;
            compiled = false;
        }
    }
    
    CHECK(ast.size() == 13);
    CHECK(ctx.vars.size() == 7);
    CHECK(ctx.funcs.size() == 5);
    CHECK(ctx.vars.at("a").value == 10);
    CHECK(ctx.vars.at("b").value == 5);
    CHECK(ctx.vars.at("res1").value == 15);
    CHECK(ctx.vars.at("res2").value == 5);
    CHECK(ctx.vars.at("res3").value == 50);
    CHECK(ctx.vars.at("res4").value == 2);
    CHECK(ctx.vars.at("res5").value == 10);

    CHECK(ss.str() == "int suma(int lvalue, int rvalue){return lvalue+rvalue;}int resta(int lvalue, int rvalue){return lvalue-rvalue;}int multiplicacion(int lvalue, int rvalue){return lvalue*rvalue;}int division(int lvalue, int rvalue){return lvalue/rvalue;}int iffun(int lvalue, int rvalue){if (lvalue>rvalue) {return lvalue;}else{return rvalue;}}int a=10;int b=5;int res1=0;int res2=0;int res3=0;int res4=0;int res5=0;void main(){res1 = suma(a, b);res2 = resta(a, b);res3 = multiplicacion(a, b);res4 = division(a, b);res5 = iffun(a, b);}");
    CHECK(parsed);
    CHECK(compiled);
    CHECK(tk == Token::Eof);
}