#include "Parser.h"

void Parser::consumeToken () {
    try {
        currToken = lexer.getNextToken();
        return;
    } catch (std::string msg) {
        std::cerr << msg << std::endl;
        throw "Unable to parse file due to a lexer error";
    }
}

UPStatementList Parser::parseCode () {
        consumeToken();
        UPStatementList astList = input();

        if (currToken == Token::Eof) {
             return std::move(astList);
        } else {
            std::cout << "Something went wrong while parsing the file" << std::endl;
        }
}

UPStatementList Parser::input () {
    UPStatementList stmts;
    while (1) {
        if (currToken == Token::Include) {
            headerDefinition();
        } else if (isTypeSpecifier(currToken) || currToken == Token::Identifier) {
            UPStatement stmt = definition();
            stmts.emplace_back(std::move(stmt));
        } else {
            break;
        }
    }
    return std::move(stmts);
}

void Parser::headerDefinition () {
    consumeToken();
    
    if (currToken == Token::StrLiteral) {
        if (!lexer.getLexeme().empty()) {
            consumeToken();
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), " \"File Name\" ", "Empty File Definition");
        }
    } else {
        error(EXPECTED_TK, lexer.getLineNo(), " \"File Name\" ", lexer.getLexeme());
    }
}

UPStatement Parser::definition () {
    if (isTypeSpecifier(currToken)) {
        std::string type = lexer.getLexeme();
        consumeToken();

        if (currToken == Token::Identifier) {
            std::string id = lexer.getLexeme();
            consumeToken();

            if (currToken == Token::OpenPar) {
                UPStatement stmt = functionDefinition(type, id);
                return std::move(stmt);
            } else if (currToken == Token::OpenBrackets || currToken == Token::OpAssign || currToken == Token::Semicolon) {
                UPStatement stmt = declaration(type, id);
                return std::move(stmt);
            }
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());
        }
    } else if (currToken == Token::Identifier) {
        std::string id = lexer.getLexeme();
        consumeToken();
        UPStatement stmt = declaration("", id);
        return std::move(stmt);
    }
}

UPStatement Parser::functionDefinition (std::string type, std::string id) {
    StringList types, ids;

    if (currToken == Token::OpenPar) {
        consumeToken();

        while (1) {
            if (isTypeSpecifier(currToken)) {
                std::string type = lexer.getLexeme();
                consumeToken();
                types.emplace_back(type);

                if (currToken == Token::Identifier) {
                    std::string id = lexer.getLexeme();
                    consumeToken();
                    ids.emplace_back(id);

                    if (currToken == Token::OpenBrackets) {
                        consumeToken();
                        expression();

                        if (currToken == Token::CloseBrackets) {
                            consumeToken();
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                        }
                    }

                    if (currToken == Token::Comma) {
                        consumeToken();

                        if (!isTypeSpecifier(currToken)) {
                            error(EXPECTED_TK, lexer.getLineNo(), "Type Specifier", lexer.getLexeme());
                        }
                    } else {
                        break;
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());
                }
            } else {
                break;
            }
        }

        if (currToken == Token::ClosePar) {
            consumeToken();
            UPStatementList stmts;
            if (currToken == Token::Semicolon) {
                consumeToken();
                return std::make_unique<FunctionDeclStatement>(type, id, types, ids, std::move(stmts), lexer.getLineNo());
            } else if (currToken == Token::OpenCurlyBraces) {
                consumeToken();
                stmts = compoundStatement();

                if (currToken == Token::CloseCurlyBraces) {
                    consumeToken();
                    return std::make_unique<FunctionDeclStatement>(type, id, types, ids, std::move(stmts), lexer.getLineNo());
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "; or {", lexer.getLexeme());
            }
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
        }
    } else {
        error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
    }
}

UPStatementList Parser::compoundStatement () {
    UPStatementList stmts;
    while (isStatement(currToken)) {
        UPStatement stmt = statement();

        if (stmt != nullptr) {
            stmts.emplace_back(std::move(stmt));
        }
    }

    return std::move(stmts);
}

UPStatement Parser::declaration (std::string type, std::string id) {
    UPExprList exprs;
    StringList ids;
    ids.emplace_back(id);
    do {
        if (currToken == Token::OpenBrackets) {
            consumeToken();

            if (isExpression(currToken)) {
                expression();

                if (currToken == Token::CloseBrackets) {
                    consumeToken();

                    if (currToken == Token::OpAssign) {
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();

                            do {
                                if (isExpression(currToken)) {
                                    expression();

                                    if (currToken == Token::Comma) {
                                        consumeToken();
                                    } else {
                                        break;
                                    }
                                }
                            } while (true);

                            if (currToken == Token::CloseCurlyBraces) {
                                consumeToken();
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), "[", lexer.getLexeme());
                        }
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
            }
        } else if (currToken == Token::OpAssign) {
            consumeToken();

            if (isExpression(currToken)) {
                UPExpr expr = expression();
                exprs.emplace_back(std::move(expr));
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
            }
        } else {
            exprs.emplace_back(nullptr);
        }

        if (currToken == Token::Comma) {
            consumeToken();

            if (currToken == Token::Identifier) {
                id = lexer.getLexeme();
                consumeToken();

                ids.emplace_back(id);
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());    
            }
        } else {
            break;
        }
    } while (true);

    if (currToken == Token::Semicolon) {
        consumeToken();
        if (type.length() != 0)
            return std::make_unique<DeclareStatement>(type, ids, std::move(exprs), lexer.getLineNo());
        else {
            return std::make_unique<AssignStatement>(id, std::move(exprs.front()), lexer.getLineNo());
        }
    } else {
        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
    }
}

UPStatement Parser::statement () {
    switch (currToken) {
        case Token::KwIf: 
        {
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();

                if (isExpression(currToken)) {
                    UPExpr cond = expression();
                    UPStatementList stmts1, stmts2;
                    if (currToken == Token::ClosePar) {
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();
                            stmts1 = compoundStatement();

                            if (currToken == Token::CloseCurlyBraces)
                                consumeToken();
                            else
                                error(EXPECTED_TK, lexer.getLineNo(), "} for if statement", lexer.getLexeme());
                        } else {
                            statement();
                        }

                        if (currToken == Token::KwElse) {
                            consumeToken();

                            if (currToken == Token::KwIf) {
                                consumeToken();

                                if (currToken == Token::OpenPar) {
                                    consumeToken();

                                    if (isExpression(currToken)) {
                                        expression();

                                        if (currToken == Token::ClosePar) {
                                            consumeToken();
                                        } else {
                                            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                                        }
                                    } else {
                                        error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                                    }
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
                                }
                            }

                            if (currToken == Token::OpenCurlyBraces) {
                                consumeToken();
                                stmts2 = compoundStatement();

                                if (currToken == Token::CloseCurlyBraces) {
                                    consumeToken();
                                    return std::make_unique<IfStatement>(std::move(cond), std::move(stmts1), std::move(stmts2));
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                                }
                            } else {
                                statement();
                            }
                        } else {
                            return std::make_unique<IfStatement>(std::move(cond), std::move(stmts1), std::move(stmts2));
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
            break;
        }
        case Token::KwFor:
        {
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();
                std::string id;
                UPExpr init;
                if (currToken == Token::Identifier) {
                    id = lexer.getLexeme();
                    consumeToken();

                    if (currToken == Token::OpAssign) {
                        consumeToken();

                        if (isExpression(currToken)) {
                            init = expression();
                        }
                    }
                }

                if (currToken == Token::Semicolon) {
                    consumeToken();

                    if (!isExpression(currToken)) {
                        error(EXPECTED_TK, lexer.getLineNo(), "expression", lexer.getLexeme());
                    }

                    UPExpr cond = expression();
                    
                    if (currToken == Token::Semicolon) {
                        consumeToken();
                        std::string incrId;
                        UPExpr incr;

                        if (currToken == Token::Identifier) {
                            incrId = lexer.getLexeme();
                            consumeToken();

                            if (currToken == Token::OpAssign) {
                                consumeToken();

                                if (isExpression(currToken)) {
                                    incr = expression();
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "expression", lexer.getLexeme());
                                }
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), "=", lexer.getLexeme());
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), "identifier", lexer.getLexeme());
                        }

                        if (currToken == Token::ClosePar) {
                            consumeToken();

                            if (currToken == Token::OpenCurlyBraces) {
                                consumeToken();
                                UPStatementList stmts = compoundStatement();

                                if (currToken == Token::CloseCurlyBraces) {
                                    consumeToken();
                                    return std::make_unique<ForStatement>(id, incrId,std::move(init), std::move(cond), std::move(incr), std::move(stmts), lexer.getLineNo());
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                                }
                            } else {
                                statement();
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
            break;
        }
        case Token::KwWhile:
        {
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();

                if (isExpression(currToken)) {
                    UPExpr cond = expression();

                    if (currToken == Token::ClosePar){
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();
                            UPStatementList stmts = compoundStatement();

                            if (currToken == Token::CloseCurlyBraces) {
                                consumeToken();
                                return std::make_unique<WhileStatement>(std::move(cond), std::move(stmts));
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                            }
                        } else {
                            statement();
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
            break;
        }
        case Token::KwBreak:
        {
            consumeToken();
            if (currToken == Token::Semicolon) {
                consumeToken();
                return std::make_unique<BreakStatement>();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
            break;
        }
        case Token::KwContinue:
        {
            consumeToken();
            if (currToken == Token::Semicolon) {
                consumeToken();
                return std::make_unique<ContinueStatement>();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
            break;
        }
        case Token::KwReturn:
        {
            consumeToken();

            if (isExpression(currToken)) {
                UPExpr expr = expression();

                if (currToken == Token::Semicolon) {
                    consumeToken();
                    return std::make_unique<ReturnStatement>(std::move(expr));
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                }
            } else if (currToken == Token::Semicolon) {
                consumeToken();
                return std::make_unique<ReturnStatement>();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
            break;
        }
        case Token::Identifier:
        {
            std::string name = lexer.getLexeme();
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();
                UPExprList parameters;

                while (isExpression(currToken)) {
                    UPExpr expr = expression();
                    parameters.emplace_back(std::move(expr));
                    if (currToken == Token::Comma) {
                        consumeToken();

                        if (!isExpression(currToken)) {
                            error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                        }
                    } else {
                        break;
                    }
                }

                if (currToken == Token::ClosePar) {
                    consumeToken();

                    if (currToken == Token::Semicolon) {
                        consumeToken();
                        return std::make_unique<FunctionStatement>(name, std::move(parameters));
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme()); 
                }
            } else if (currToken == Token::OpenBrackets) {
                consumeToken();

                if (isExpression(currToken)) {
                    UPExpr indx = expression();

                    if (currToken == Token::CloseBrackets) {
                        consumeToken();

                        if (currToken == Token::OpAssign) {
                            consumeToken();
                            UPExpr expr = expression();

                            if (currToken == Token::Semicolon) {
                                consumeToken();
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                            }
                        } else if (currToken == Token::Semicolon) {
                            consumeToken();
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "expression", lexer.getLexeme());
                }
            } else if (currToken == Token::OpAssign) {
                consumeToken();

                if (isExpression(currToken)) {
                    UPExpr expr = expression();

                    if (currToken == Token::Semicolon) {
                        consumeToken();

                        return std::make_unique<AssignStatement>(name, std::move(expr), lexer.getLineNo());
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                }
            }
            break;
        }
        default:
        {
            if (isTypeSpecifier(currToken)) {
                std::string type = lexer.getLexeme();
                consumeToken();

                if (currToken == Token::Identifier) {
                    std::string id = lexer.getLexeme();
                    consumeToken();

                    if (currToken == Token::OpAssign || currToken == Token::Semicolon) {
                        UPStatement stmt = declaration(type, id);
                        return std::move(stmt);
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), "= or ;", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());
                }
            } 
            break;
        }
    }
    return nullptr;
}

UPExpr Parser::expression () {
    if (currToken == Token::KwTrue || currToken == Token::KwFalse) {
        int value = (currToken == Token::KwTrue) ? 1 : 0;
        consumeToken();

        return std::make_unique<BoolExpr>(value);
    } else {
        return logicalTerm();
    }
}

UPExpr Parser::logicalTerm () {
    UPExpr lvalue = bitwiseTerm();

    while (currToken == Token::OpAnd || currToken == Token::OpOr) {
        if (currToken == Token::OpAnd) {
            consumeToken();
            UPExpr rvalue = bitwiseTerm();

            lvalue = std::make_unique<AndExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpOr) {
            consumeToken();
            UPExpr rvalue = bitwiseTerm();

            lvalue = std::make_unique<OrExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::bitwiseTerm () {
    UPExpr lvalue = relationalTerm();

    while (currToken == Token::OpBitwiseOr || currToken == Token::OpBitwiseXor || currToken == Token::OpBitwiseAnd) {
        if (currToken == Token::OpBitwiseOr) {
            consumeToken();
            UPExpr rvalue = relationalTerm();

            lvalue = std::make_unique<BitwiseOrExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpBitwiseXor) {
            consumeToken();
            UPExpr rvalue = relationalTerm();

            lvalue = std::make_unique<BitwiseXorExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpBitwiseAnd) {
            consumeToken();
            UPExpr rvalue = relationalTerm();

            lvalue = std::make_unique<BitwiseAndExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::relationalTerm () {
    UPExpr lvalue = relationalGLETerm();

    while (currToken == Token::OpEqual || currToken == Token::OpNotEqual) {
        if (currToken == Token::OpEqual) {
            consumeToken();
            UPExpr rvalue = relationalGLETerm();

            lvalue = std::make_unique<EqualExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpNotEqual) {
            consumeToken();
            UPExpr rvalue = relationalGLETerm();

            lvalue = std::make_unique<NotEqualExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::relationalGLETerm () {
    UPExpr lvalue = bitwiseShiftTerm();

    while (currToken == Token::OpGreater || currToken == Token::OpGreaterEqual || currToken == Token::OpLess || currToken == Token::OpLessEqual) {
        if (currToken == Token::OpGreater) {
            consumeToken();
            UPExpr rvalue = bitwiseShiftTerm();

            lvalue = std::make_unique<GreaterExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpGreaterEqual) {
            consumeToken();
            UPExpr rvalue = bitwiseShiftTerm();

            lvalue = std::make_unique<GreaterEqualExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpLess) {
            consumeToken();
            UPExpr rvalue = bitwiseShiftTerm();

            lvalue = std::make_unique<LessExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpLessEqual) {
            consumeToken();
            UPExpr rvalue = bitwiseShiftTerm();

            lvalue = std::make_unique<LessEqualExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::bitwiseShiftTerm () {
    UPExpr lvalue = addSubTerm();

    while (currToken == Token::OpShiftLeft || currToken == Token::OpShiftRight) {
        if (currToken == Token::OpShiftLeft) {
            consumeToken();
            UPExpr rvalue = addSubTerm();

            lvalue = std::make_unique<ShiftLeftExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpShiftRight) {
            consumeToken();
            UPExpr rvalue = addSubTerm();

            lvalue = std::make_unique<ShiftRightExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::addSubTerm () {
    UPExpr lvalue = mulDivModTerm();

    while (currToken == Token::OpAdd || currToken == Token::OpSub) {
        if (currToken == Token::OpAdd) {
            consumeToken();
            UPExpr rvalue = mulDivModTerm();

            lvalue = std::make_unique<AddExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpSub) {
            consumeToken();
            UPExpr rvalue = mulDivModTerm();

            lvalue = std::make_unique<SubExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::mulDivModTerm () {
    UPExpr lvalue = factor();

    while (currToken == Token::OpMul || currToken == Token::OpDiv || currToken == Token::OpMod) {
        if (currToken == Token::OpMul) {
            consumeToken();
            UPExpr rvalue = factor();

            lvalue = std::make_unique<MulExpr>(std::move(lvalue), std::move(rvalue));
        } else if (currToken == Token::OpDiv) {
            consumeToken();
            UPExpr rvalue = factor();

            lvalue = std::make_unique<DivExpr>(std::move(lvalue), std::move(rvalue));
        } else {
            consumeToken();
            UPExpr rvalue = factor();

            lvalue = std::make_unique<ModExpr>(std::move(lvalue), std::move(rvalue));
        }
    }

    return std::move(lvalue);
}

UPExpr Parser::factor() {
    if (currToken == Token::CharLiteral) {
        char value = lexer.getLexeme().at(0);
        consumeToken();

        return std::make_unique<CharExpr>(value);
    } else if (currToken == Token::OpenPar) {
        consumeToken();
        UPExpr expr = expression();

        if (currToken == Token::ClosePar) {
            consumeToken();
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
        }

        return std::move(expr);
    } else if (isExpression(currToken)) {
        if (currToken == Token::OpLogicalNot) {
            consumeToken();
        } else if (currToken == Token::OpBitwiseNot) {
            consumeToken();
        } else if (currToken == Token::OpIncrement) {
            consumeToken();
        } else if (currToken == Token::OpDecrement) {
            consumeToken();
        }

        if (currToken == Token::Decimal || currToken == Token::Binary || currToken == Token::Hexadecimal || currToken == Token::Octal) {
            int value = stoi(lexer.getLexeme());            
            consumeToken();

            return std::make_unique<NumExpr>(value);
        } else if (currToken == Token::Identifier) {
            std::string name = lexer.getLexeme();
            consumeToken();

            if (currToken == Token::OpenBrackets) {
                consumeToken();
                expression();

                if (currToken == Token::CloseBrackets) {
                    consumeToken();
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                }
            } else if (currToken == Token::OpIncrement) {
                consumeToken();
            } else if (currToken == Token::OpDecrement) {
                consumeToken();
            } else if (currToken == Token::OpenPar) {
                consumeToken();
                UPExprList parameters;

                while (isExpression(currToken)) {
                    UPExpr expr = expression();
                    parameters.emplace_back(std::move(expr));
                    if (currToken == Token::Comma) {
                        consumeToken();

                        if (!isExpression(currToken)) {
                            error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                        }
                    } else {
                        break;
                    }
                }

                if (currToken == Token::ClosePar) {
                    consumeToken();
                    return std::make_unique<FunctionExpr>(name, std::move(parameters));

                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme()); 
                }
            }

            return std::make_unique<IdExpr>(name);
        }
    }
    return nullptr;
}