#ifndef LEXER_H
#define LEXER_H

#include <fstream>
#include "string.h"
#include <iostream>
#include <functional>

#include "Token.h"
#include "Utilities.h"

class Lexer {
public:
	Lexer (std::istream &in) : in(in) {
		getNextSymbol();
		lineNo = 1;
	}

	Token getNextToken();
	std::string getLexeme() { return lexeme; }
	int getLineNo() { return lineNo; }

private:
	void getNextSymbol() { currSym = in.get();	}
	void ungetSymbol(char ch) { currSym = ch; in.unget(); }
	void append(std::function <bool(char)> func);
	bool ignore();
	Token lookUpKeyword();
	Token lookUpIntConstant();
private:
	char currSym;
	std::string lexeme;
	int lineNo;
	std::istream &in;
};

#endif