#include "Utilities.h"

void error (int errorNo, int lineNo, std::string s1, std::string s2) {
    switch (errorNo) {
        case 0: throw "Line:" + std::to_string(lineNo) + ": Error unknown symbol " + s2;
        case 1: throw "Line:" + std::to_string(lineNo) + ": Error unclosed token expected: '" + s1 + "' but found '" + s2 + "'";
        case 2: throw "Line:" + std::to_string(lineNo) + ": Error unknown int constant: '" + s2 + "'";
    }    
}