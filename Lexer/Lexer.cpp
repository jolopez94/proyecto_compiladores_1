#include "Lexer.h"

#define SIMPLE_TOKEN(tk)	                                                        \
	do {					                                                        \
		lexeme = currSym;	                                                        \
		getNextSymbol();	                                                        \
		return tk;			                                                        \
	} while (0)

#define COMPOUND_TOKEN(tk1, tk2, sym1, sym2)                                        \
	do {					                                                        \
		getNextSymbol();	                                                        \
        if (currSym == sym2) {                                                      \
            lexeme = std::string() + sym1 + sym2;                                   \
            getNextSymbol();                                                        \
            return tk2;                                                             \
        }                                                                           \
        lexeme = sym1;                                                              \
        return tk1;                                                                 \
	} while (0)

#define COMPLEX_TOKEN(sym)                                                          \
    do {                                                                            \
        getNextSymbol();                                                            \
        if (currSym == sym) {                                                       \
            lexeme = std::string() + sym + sym;                                     \
            getNextSymbol();                                                        \
            return ('<' == sym) ? Token::OpShiftLeft : Token::OpShiftRight;         \
        } else if (currSym == '=') {                                                \
            lexeme = std::string() + sym + '=';                                     \
            getNextSymbol();                                                        \
            return ('<' == sym) ? Token::OpLessEqual : Token::OpGreaterEqual;       \
        }                                                                           \
        lexeme = sym;                                                               \
        return ('<' == sym) ? Token::OpLess : Token::OpGreater;                     \
    } while (0)

Token Lexer::lookUpKeyword() {
	for (int i = 0; kw[i] != 0; i++) {
		if (strcasecmp(kw[i], lexeme.c_str()) == 0)
			return kwTk[i];
	}

	return Token::Identifier;
}

Token Lexer::lookUpIntConstant() {	
	if (currSym == '0') {
		lexeme += currSym;
		getNextSymbol();

		if (currSym == 'x' || currSym == 'X') {
			lexeme += currSym;
			getNextSymbol();

			if (isxdigit(currSym)) {
				append( [](char ch){ return isxdigit(ch); } );
				return Token::Hexadecimal;
			} else {
				return Token::Undefined;
			}
		} else if (currSym == 'b' || currSym == 'B') {
			lexeme += currSym;			
			getNextSymbol();

			if (currSym == '0' || currSym == '1') {
				append( [](char ch){ return (ch == '0' || ch == '1'); } );
				return Token::Binary;
			} else {
				return Token::Undefined;
			}
		} else if (isoctal(currSym)) {			
			append( [](char ch){ return isoctal(ch); } );
			return Token::Octal;
		} else {
			lexeme = "0";
			return Token::Decimal;
		}
	} else {
		append( [](char ch){ return isdigit(ch); } );
		return Token::Decimal;
	}
}

void Lexer::append(std::function <bool(char)> func) {
	while (func(currSym)) {
		lexeme += currSym;
		getNextSymbol();		
	}
}

bool Lexer::ignore() {
	if (currSym == '\n') {
		lineNo++;
		getNextSymbol();
		return true;
	} else if (currSym == ' ' || currSym == '\t') {
		getNextSymbol();
		return true;
	} else if (currSym == '/') {
		getNextSymbol();
		
		if (currSym == '/') {
			while (currSym != '\n' && currSym != EOF)
				getNextSymbol();
			return true;

		} else if (currSym == '*') {
			getNextSymbol();

			while (1) {
				if (currSym == '*') {
					getNextSymbol();

					if (currSym == '/') {
						getNextSymbol();
						return true;
					}
				} else if (currSym == EOF) {
					error(UNCLOSED_TK, lineNo, "*/", "End Of File");
				} else {
					getNextSymbol();
				}
			}
		}
		ungetSymbol('/');
	}
	return false;
}

Token Lexer::getNextToken() {
	lexeme = "";

	while (1) {
		if (ignore())
			continue;

		switch (currSym) {
			case '+' : COMPOUND_TOKEN(Token::OpAdd, Token::OpIncrement, '+', '+');
			case '-' : COMPOUND_TOKEN(Token::OpSub, Token::OpDecrement, '-', '-');
			case '*' : SIMPLE_TOKEN(Token::OpMul);
			case '/' : SIMPLE_TOKEN(Token::OpDiv);
            case '%' : SIMPLE_TOKEN(Token::OpMod);
			case '=' : COMPOUND_TOKEN(Token::OpAssign, Token::OpEqual, '=', '=');
            case '!' : COMPOUND_TOKEN(Token::OpLogicalNot, Token::OpNotEqual, '!', '=');
            case '<' : COMPLEX_TOKEN('<');
            case '>' : COMPLEX_TOKEN('>');
            case '&' : COMPOUND_TOKEN(Token::OpBitwiseAnd, Token::OpAnd, '&', '&');
            case '|' : COMPOUND_TOKEN(Token::OpBitwiseOr, Token::OpOr, '|', '|');
            case '^' : SIMPLE_TOKEN(Token::OpBitwiseXor);
            case '~' : SIMPLE_TOKEN(Token::OpBitwiseNot);
			case '(' : SIMPLE_TOKEN(Token::OpenPar);
			case ')' : SIMPLE_TOKEN(Token::ClosePar);
			case '{' : SIMPLE_TOKEN(Token::OpenCurlyBraces);
			case '}' : SIMPLE_TOKEN(Token::CloseCurlyBraces);
			case '[' : SIMPLE_TOKEN(Token::OpenBrackets);
			case ']' : SIMPLE_TOKEN(Token::CloseBrackets);
			case ',' : SIMPLE_TOKEN(Token::Comma);
            case ';' : SIMPLE_TOKEN(Token::Semicolon);
			case '#' :
				lexeme = currSym;
				getNextSymbol();
				append( [](char ch){ return isalpha(ch); } );

				if (strcasecmp("#include", lexeme.c_str()) == 0)
					return Token::Include;
				error(UNKNOWN_TK, lineNo, "", lexeme);
            case '"' : 
                getNextSymbol();
                append( [](char ch) { 	if (ch == '"' || ch == EOF)
											return false;
										return true;
									} 
				);

                if (currSym == '"') {
                    getNextSymbol();
                    return Token::StrLiteral;
                } else {
                    error(UNCLOSED_TK, lineNo, "\"", "End Of File");
                }
            case '\'':
                getNextSymbol();
                lexeme = currSym;
				getNextSymbol();
				
                if (currSym == '\'') {
                    getNextSymbol();
                    return Token::CharLiteral;
                } else {
                    error(UNCLOSED_TK, lineNo, "'", std::string(1, currSym));
                }
			case EOF : SIMPLE_TOKEN(Token::Eof);
			default:
				if (isdigit(currSym)) {
					Token aux = lookUpIntConstant();

					if (aux == Token::Undefined)
						error(INT_CONSTNT, lineNo, "", lexeme);
					else
						return aux;
				} else if (isalpha(currSym) || currSym == '_') {
					append( [](char ch){ return isalpha(ch) || (ch == '_') || isdigit(ch); } );
					return lookUpKeyword();
				} else {
					std::string unkSym = std::string(1, currSym);
					getNextSymbol();
					error(UNKNOWN_TK, lineNo, "", unkSym);
				}
		}
	}
}