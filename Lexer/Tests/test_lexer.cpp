#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <sstream>
#include <memory>
#include <cstring>
#include "doctest.h"
#include "Lexer.h"

/**
 * Keywords
 **/
const char *test1 = "int void char bool true false";
const char *test2 = "break continue else for do return while if";
/**
 * Operators
 **/
const char *test3 = "+ - * / % ++ --";
const char *test4 = "== != > < >= <=";
const char *test5 = "&& || !";
const char *test6 = "& | ^ ~ >> <<";
const char *test7 = "identifier = expression + 1 ;";
/**
 * Comments and Punctuations
 **/
const char *test8 = "() {} [] , ;";
const char *test9 = "//Line comment\n"
                    "45 // Line Comment\n"
                    "+ //               Line Comment\n"
                    "10 //                       Line Comment\n"
                    "//";
const char *test10 = "/* Block comment /*\n"
                     "   Block comment */ 45 /*** Block comment ***/ + /* Block comment /*88*/ ident /**/";
/**
 * Constants, Identifiers and Others
 **/
const char *test11 = "#include identifier 1918379172 0b010101 0xabc123 07654321 'c' \"This is a string\"";
/**
 * Errors 
 **/
const char *test12 = "/* Unknown symbols */ $ @ `\n"
                     "?";
const char *test13 = "'c/* Unclosed comment *";
const char *test14 = "\"Unclosed string";
const char *test15 = "0b 0x";

void consumeToken (Lexer &l, Token &tk) {
    try {
        tk = l.getNextToken();
    } catch (std::string msg) {
        std::cerr << msg << std::endl;
    }
}

// const char *test1 = "int void char";

TEST_CASE("Keywords Test: type identifiers") {
    std::istringstream in;

    in.str(test1);
	Lexer l(in);
    Token tk;
    
    consumeToken(l, tk);
    CHECK(tk == Token::KwInt);
    CHECK(l.getLexeme() == "int");
    consumeToken(l, tk);
    CHECK(tk == Token::KwVoid);
    CHECK(l.getLexeme() == "void");
    consumeToken(l, tk);
    CHECK(tk == Token::KwChar);
    CHECK(l.getLexeme() == "char");
    consumeToken(l, tk);
    CHECK(tk == Token::KwBool);
    CHECK(l.getLexeme() == "bool");
    consumeToken(l, tk);
    CHECK(tk == Token::KwTrue);
    CHECK(l.getLexeme() == "true");
    consumeToken(l, tk);
    CHECK(tk == Token::KwFalse);
    CHECK(l.getLexeme() == "false");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test2 = "break continue else for do return while if";

TEST_CASE("Keywords Test: statements") {
    std::istringstream in;

    in.str(test2);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::KwBreak);
    CHECK(l.getLexeme() == "break");
    consumeToken(l, tk);
    CHECK(tk == Token::KwContinue);
    CHECK(l.getLexeme() == "continue");
    consumeToken(l, tk);
    CHECK(tk == Token::KwElse);
    CHECK(l.getLexeme() == "else");
    consumeToken(l, tk);
    CHECK(tk == Token::KwFor);
    CHECK(l.getLexeme() == "for");
    consumeToken(l, tk);
    CHECK(tk == Token::KwDo);
    CHECK(l.getLexeme() == "do");
    consumeToken(l, tk);
    CHECK(tk == Token::KwReturn);
    CHECK(l.getLexeme() == "return");
    consumeToken(l, tk);
    CHECK(tk == Token::KwWhile);
    CHECK(l.getLexeme() == "while");
    consumeToken(l, tk);
    CHECK(tk == Token::KwIf);
    CHECK(l.getLexeme() == "if");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test3 = "+ - * / % ++ --";

TEST_CASE("Operators Test: arithmetic") {
    std::istringstream in;

    in.str(test3);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::OpAdd);
    CHECK(l.getLexeme() == "+");
    consumeToken(l, tk);
    CHECK(tk == Token::OpSub);
    CHECK(l.getLexeme() == "-");
    consumeToken(l, tk);
    CHECK(tk == Token::OpMul);
    CHECK(l.getLexeme() == "*");
    consumeToken(l, tk);
    CHECK(tk == Token::OpDiv);
    CHECK(l.getLexeme() == "/");
    consumeToken(l, tk);
    CHECK(tk == Token::OpMod);
    CHECK(l.getLexeme() == "%");
    consumeToken(l, tk);
    CHECK(tk == Token::OpIncrement);
    CHECK(l.getLexeme() == "++");
    consumeToken(l, tk);
    CHECK(tk == Token::OpDecrement);
    CHECK(l.getLexeme() == "--");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test4 = "== != > < >= <=";

TEST_CASE("Operators Test: relational") {
    std::istringstream in;

    in.str(test4);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::OpEqual);
    CHECK(l.getLexeme() == "==");
    consumeToken(l, tk);
    CHECK(tk == Token::OpNotEqual);
    CHECK(l.getLexeme() == "!=");
    consumeToken(l, tk);
    CHECK(tk == Token::OpGreater);
    CHECK(l.getLexeme() == ">");
    consumeToken(l, tk);
    CHECK(tk == Token::OpLess);
    CHECK(l.getLexeme() == "<");
    consumeToken(l, tk);
    CHECK(tk == Token::OpGreaterEqual);
    CHECK(l.getLexeme() == ">=");
    consumeToken(l, tk);
    CHECK(tk == Token::OpLessEqual);
    CHECK(l.getLexeme() == "<=");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test5 = "&& || !";

TEST_CASE("Operators Test: logical") {
    std::istringstream in;

    in.str(test5);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::OpAnd);
    CHECK(l.getLexeme() == "&&");
    consumeToken(l, tk);
    CHECK(tk == Token::OpOr);
    CHECK(l.getLexeme() == "||");
    consumeToken(l, tk);
    CHECK(tk == Token::OpLogicalNot);
    CHECK(l.getLexeme() == "!");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test6 = "& | ^ ~ >> <<";

TEST_CASE("Operators Test: bitwise") {
    std::istringstream in;

    in.str(test6);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::OpBitwiseAnd);
    CHECK(l.getLexeme() == "&");
    consumeToken(l, tk);
    CHECK(tk == Token::OpBitwiseOr);
    CHECK(l.getLexeme() == "|");
    consumeToken(l, tk);
    CHECK(tk == Token::OpBitwiseXor);
    CHECK(l.getLexeme() == "^");
    consumeToken(l, tk);
    CHECK(tk == Token::OpBitwiseNot);
    CHECK(l.getLexeme() == "~");
    consumeToken(l, tk);
    CHECK(tk == Token::OpShiftRight);
    CHECK(l.getLexeme() == ">>");
    consumeToken(l, tk);
    CHECK(tk == Token::OpShiftLeft);
    CHECK(l.getLexeme() == "<<");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test7 = "identifier = expression + 1 ;";

TEST_CASE("Operators Test: assign") {
    std::istringstream in;

    in.str(test7);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::Identifier);
    CHECK(l.getLexeme() == "identifier");
    consumeToken(l, tk);
    CHECK(tk == Token::OpAssign);
    CHECK(l.getLexeme() == "=");
    consumeToken(l, tk);
    CHECK(tk == Token::Identifier);
    CHECK(l.getLexeme() == "expression");
    consumeToken(l, tk);
    CHECK(tk == Token::OpAdd);
    CHECK(l.getLexeme() == "+");
    consumeToken(l, tk);
    CHECK(tk == Token::Decimal);
    CHECK(l.getLexeme() == "1");
    consumeToken(l, tk);
    CHECK(tk == Token::Semicolon);
    CHECK(l.getLexeme() == ";");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test8 = "() {} [] , ;";

TEST_CASE("Punctuations Test: punctuations") {
    std::istringstream in;

    in.str(test8);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::OpenPar);
    CHECK(l.getLexeme() == "(");
    consumeToken(l, tk);
    CHECK(tk == Token::ClosePar);
    CHECK(l.getLexeme() == ")");
    consumeToken(l, tk);
    CHECK(tk == Token::OpenCurlyBraces);
    CHECK(l.getLexeme() == "{");
    consumeToken(l, tk);
    CHECK(tk == Token::CloseCurlyBraces);
    CHECK(l.getLexeme() == "}");
    consumeToken(l, tk);
    CHECK(tk == Token::OpenBrackets);
    CHECK(l.getLexeme() == "[");
    consumeToken(l, tk);
    CHECK(tk == Token::CloseBrackets);
    CHECK(l.getLexeme() == "]");
    consumeToken(l, tk);
    CHECK(tk == Token::Comma);
    CHECK(l.getLexeme() == ",");
    consumeToken(l, tk);
    CHECK(tk == Token::Semicolon);
    CHECK(l.getLexeme() == ";");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test9 = "//Line comment\n"
//                     "45 // Line Comment\n"
//                     "+ //               Line Comment\n"
//                     "10 //                       Line Comment\n"
//                     "//";

TEST_CASE("Comments Test: line comments") {
    std::istringstream in;

    in.str(test9);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::Decimal);
    CHECK(l.getLexeme() == "45");
    consumeToken(l, tk);
    CHECK(tk == Token::OpAdd);
    CHECK(l.getLexeme() == "+");
    consumeToken(l, tk);
    CHECK(tk == Token::Decimal);
    CHECK(l.getLexeme() == "10");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
    CHECK(l.getLineNo() == 5);
}

// const char *test10 = "/* Block comment /*\n"
//                      "   Block comment */ 45 /*** Block comment ***/ + /* Block comment /*88*/ ident /**/";

TEST_CASE("Comments Test: block comments") {
    std::istringstream in;

    in.str(test10);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::Decimal);
    CHECK(l.getLexeme() == "45");
    consumeToken(l, tk);
    CHECK(tk == Token::OpAdd);
    CHECK(l.getLexeme() == "+");
    consumeToken(l, tk);
    CHECK(tk == Token::Identifier);
    CHECK(l.getLexeme() == "ident");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test11 = "#include identifier 1918379172 0b010101 0xabc123 07654321 'c' \"This is a string\"";

TEST_CASE("Identifiers and constants ") {
    std::istringstream in;

    in.str(test11);
    Lexer l(in);
    Token tk;

    consumeToken(l, tk);
    CHECK(tk == Token::Include);
    CHECK(l.getLexeme() == "#include");
    consumeToken(l, tk);
    CHECK(tk == Token::Identifier);
    CHECK(l.getLexeme() == "identifier");
    consumeToken(l, tk);
    CHECK(tk == Token::Decimal);
    CHECK(l.getLexeme() == "1918379172");
    consumeToken(l, tk);
    CHECK(tk == Token::Binary);
    CHECK(l.getLexeme() == "0b010101");
    consumeToken(l, tk);
    CHECK(tk == Token::Hexadecimal);
    CHECK(l.getLexeme() == "0xabc123");
    consumeToken(l, tk);
    CHECK(tk == Token::Octal);
    CHECK(l.getLexeme() == "07654321");
    consumeToken(l, tk);
    CHECK(tk == Token::CharLiteral);
    CHECK(l.getLexeme() == "c");
    consumeToken(l, tk);
    CHECK(tk == Token::StrLiteral);
    CHECK(l.getLexeme() == "This is a string");
    consumeToken(l, tk);
    CHECK(tk == Token::Eof);
}

// const char *test12 = "/* Unknown symbols */ $ @ `";

TEST_CASE("Unknown symbols") {
    std::istringstream in;

    in.str(test12);
    Lexer l(in);
    Token tk;
    int count = 0;

    do {
        try {
            tk = l.getNextToken();
        } catch (std::string msg) {
            count++;
            tk = Token::Undefined;
            std::cerr << msg << std::endl;
        }
    } while (tk != Token::Eof);

    CHECK(l.getLineNo() == 2);
    CHECK(count == 4);
}

// const char *test13 = "/* Unclosed comment *";

TEST_CASE("Unclosed comment") {
    std::istringstream in;

    in.str(test13);
    Lexer l(in);
    Token tk;
    int count = 0;

    do {
        try {
            tk = l.getNextToken();
        } catch (std::string msg) {
            count++;
            tk = Token::Undefined;
            std::cerr << msg << std::endl;
        }
    } while (tk != Token::Eof);

    CHECK(l.getLineNo() == 1);
    CHECK(count == 2);
}

// const char *test14 = "\"Unclosed string";

TEST_CASE("Unclosed string") {
    std::istringstream in;

    in.str(test14);
    Lexer l(in);
    Token tk;
    int count = 0;

    do {
        try {
            tk = l.getNextToken();
        } catch (std::string msg) {
            count++;
            tk = Token::Undefined;
            std::cerr << msg << std::endl;
        }
    } while (tk != Token::Eof);

    CHECK(l.getLineNo() == 1);
    CHECK(count == 1);
}

// const char *test15 = "0b 0x";

TEST_CASE("Unknown int constants") {
    std::istringstream in;

    in.str(test15);
    Lexer l(in);
    Token tk;
    int count = 0;

    do {
        try {
            tk = l.getNextToken();
        } catch (std::string msg) {
            count++;
            tk = Token::Undefined;
            std::cerr << msg << std::endl;
        }
    } while (tk != Token::Eof);

    CHECK(l.getLineNo() == 1);
    CHECK(count == 2);
}