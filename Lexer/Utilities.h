#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>

#define UNKNOWN_TK  0
#define UNCLOSED_TK 1
#define INT_CONSTNT 2 

/* Declared Functions */

void error (int errorNo, int lineNo, std::string s1, std::string s2);

/* Defined Functions */

static inline bool isoctal (char ch) {
    return ch >= '0' && ch <= '7';
}

#endif
