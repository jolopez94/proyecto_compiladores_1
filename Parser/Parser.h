#ifndef PARSER_H
#define PARSER_H

#include "Lexer.h"

class Parser {
public:
	Parser(Lexer &lexer): lexer (lexer) { }
	void parseCode();
    Token getToken() { return currToken; }
    
private:
	Token currToken;
	Lexer &lexer;

    void consumeToken();
	void input();
    void headerDefinition();
    void definition();
	void functionDefinition();
    void parameterList();
    void compoundStatement();
    void declaration();
    void statement();
    void expression();
    void logicalTerm();
    void bitwiseTerm();
    void relationalTerm();
    void relationalGLETerm();
    void bitwiseShiftTerm();
    void addSubTerm();
    void mulDivModTerm();
    void factor();
};

#endif