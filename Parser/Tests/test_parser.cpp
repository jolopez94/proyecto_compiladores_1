#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <sstream>
#include <memory>
#include <cstring>
#include "doctest.h"
#include "Parser.h"

void consumeToken (Lexer &l, Token &tk) {
    try {
        tk = l.getNextToken();
    } catch (std::string msg) {
        std::cerr << msg << std::endl;
    }
}

const char *test1 = "#include \"stdio.h\" \n"
                    "#include \"Anyfile.txt\"\n"
                    "int var = 10;\n"
                    "char var = 'c';\n"
                    "int suma (int a, int b);\n"
                    "int var[1] = {2};\n"
                    "int suma (int a, int b) { return 1 + 2;\n"
                    "for (1; 1<1; id++){id();} }\n"
                    "int var;";

TEST_CASE("Programs") {
    std::istringstream in;

    in.str(test1);
	Lexer l(in);
    Parser p(l);
    bool parsed;

    try {
        p.parseCode();
        parsed = true;
    } catch (std::string msg) {
        parsed = false;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    CHECK(parsed);
    CHECK(tk == Token::Eof);
}

const char *test2 = "int main(int v, ){}";

TEST_CASE("Parameter") {
    std::istringstream in;

    in.str(test2);
	Lexer l(in);
    Parser p(l);
    bool parsed;

    try {
        p.parseCode();
        parsed = false;
    } catch (std::string msg) {
        parsed = true;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    CHECK(parsed);
    CHECK(tk == Token::ClosePar);
}

const char *test3 = "int main(int v){\n"
                    "for (1; 1<1 id++){}}";

TEST_CASE("For") {
    std::istringstream in;

    in.str(test3);
	Lexer l(in);
    Parser p(l);
    bool parsed;

    try {
        p.parseCode();
        parsed = false;
    } catch (std::string msg) {
        parsed = true;
        std::cerr << msg << std::endl;
    }

    Token tk = p.getToken();
    
    CHECK(parsed);
    CHECK(tk == Token::Identifier);
}