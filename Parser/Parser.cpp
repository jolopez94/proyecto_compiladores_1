#include "Parser.h"

void Parser::consumeToken () {
    do {
        try {
            currToken = lexer.getNextToken();
            return;
        } catch (std::string msg) {
            std::cerr << msg << std::endl;
        }
    } while (true);
}

void Parser::parseCode () {
        consumeToken();
        input();

        if (currToken == Token::Eof) {
            std::cout << "File parsed successfully" << std::endl;
        } else {
            std::cout << "Something went wrong while parsing the file" << std::endl;
        }
}

void Parser::input () {
    while (1) {
        if (currToken == Token::Include) {
            headerDefinition();
        } else if (isTypeSpecifier(currToken) || currToken == Token::Identifier) {
            definition();
        } else {
            break;
        }
    }
}

void Parser::headerDefinition () {
    consumeToken();
    
    if (currToken == Token::StrLiteral) {
        if (!lexer.getLexeme().empty()) {
            consumeToken();
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), " \"File Name\" ", "Empty File Definition");
        }
    } else {
        error(EXPECTED_TK, lexer.getLineNo(), " \"File Name\" ", lexer.getLexeme());
    }
}

void Parser::definition () {
    if (isTypeSpecifier(currToken)) {
        consumeToken();

        if (currToken == Token::Identifier) {
            consumeToken();

            if (currToken == Token::OpenPar) {
                functionDefinition();
            } else if (currToken == Token::OpenBrackets || currToken == Token::OpAssign || currToken == Token::Semicolon) {
                declaration();
            }
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());
        }
    } else if (currToken == Token::Identifier) {
        consumeToken();
        declaration();
    }
}

void Parser::functionDefinition () {
    if (currToken == Token::OpenPar) {
        consumeToken();
        parameterList();

        if (currToken == Token::ClosePar) {
            consumeToken();

            if (currToken == Token::Semicolon) {
                consumeToken();
            } else if (currToken == Token::OpenCurlyBraces) {
                consumeToken();
                compoundStatement();

                if (currToken == Token::CloseCurlyBraces) {
                    consumeToken();
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "; or {", lexer.getLexeme());
            }
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
        }
    } else {
        error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
    }
}

void Parser::parameterList () {
    while (1) {
        if (isTypeSpecifier(currToken)) {
            consumeToken();

            if (currToken == Token::Identifier) {
                consumeToken();

                if (currToken == Token::OpenBrackets) {
                    consumeToken();
                    expression();

                    if (currToken == Token::CloseBrackets) {
                        consumeToken();
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                    }
                }

                if (currToken == Token::Comma) {
                    consumeToken();

                    if (!isTypeSpecifier(currToken)) {
                        error(EXPECTED_TK, lexer.getLineNo(), "Type Specifier", lexer.getLexeme());
                    }
                } else {
                    return;
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());
            }
        } else {
            return;
        }
    }
}

void Parser::compoundStatement () {
    while (isStatement(currToken)) {
        statement();
    }
}

//<Declaration> ::= 
//{{ [ <Expression> ] = { <Expression> { , <Expression>}* } } 
//| { = <Expression> }}+ 
//{, identifier {{ [ <Expression> ] = { <Expression> { , <Expression>}* } } | { = <Expression> }}+}* ;
void Parser::declaration () {
    do {
        if (currToken == Token::OpenBrackets) {
            consumeToken();

            if (isExpression(currToken)) {
                expression();

                if (currToken == Token::CloseBrackets) {
                    consumeToken();

                    if (currToken == Token::OpAssign) {
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();

                            do {
                                if (isExpression(currToken)) {
                                    expression();

                                    if (currToken == Token::Comma) {
                                        consumeToken();
                                    } else {
                                        break;
                                    }
                                }
                            } while (true);

                            if (currToken == Token::CloseCurlyBraces) {
                                consumeToken();
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), "[", lexer.getLexeme());
                        }
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
            }
        } else if (currToken == Token::OpAssign) {
            consumeToken();

            if (isExpression(currToken)) {
                expression();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
            }
        }

        if (currToken == Token::Comma) {
            consumeToken();

            if (currToken == Token::Identifier) {
                consumeToken();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "Identifier", lexer.getLexeme());    
            }
        } else {
            break;
        }
    } while (true);

    if (currToken == Token::Semicolon)
        consumeToken();
    else
        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
}

void Parser::statement () {
    switch (currToken) {
        case Token::KwIf:
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();

                if (isExpression(currToken)) {
                    expression();

                    if (currToken == Token::ClosePar) {
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();
                            compoundStatement();

                            if (currToken == Token::CloseCurlyBraces)
                                consumeToken();
                            else
                                error(EXPECTED_TK, lexer.getLineNo(), "} for if statement", lexer.getLexeme());
                        } else {
                            statement();
                        }

                        if (currToken == Token::KwElse) {
                            consumeToken();

                            if (currToken == Token::KwIf) {
                                consumeToken();

                                if (currToken == Token::OpenPar) {
                                    consumeToken();

                                    if (isExpression(currToken)) {
                                        expression();

                                        if (currToken == Token::ClosePar) {
                                            consumeToken();
                                        } else {
                                            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                                        }
                                    } else {
                                        error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                                    }
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
                                }
                            }

                            if (currToken == Token::OpenCurlyBraces) {
                                consumeToken();
                                compoundStatement();

                                if (currToken == Token::CloseCurlyBraces) {
                                    consumeToken();
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                                }
                            } else {
                                statement();
                            }
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
        break;
        case Token::KwFor:
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();
                expression();

                if (currToken == Token::Semicolon) {
                    consumeToken();
                    expression();
                    
                    if (currToken == Token::Semicolon) {
                        consumeToken();
                        expression();

                        if (currToken == Token::ClosePar) {
                            consumeToken();

                            if (currToken == Token::OpenCurlyBraces) {
                                consumeToken();
                                compoundStatement();

                                if (currToken == Token::CloseCurlyBraces) {
                                    consumeToken();
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), "}", lexer.getLexeme());
                                }
                            } else {
                                statement();
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
        break;
        case Token::KwWhile:
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();

                if (isExpression(currToken)) {
                    expression();

                    if (currToken == Token::ClosePar){
                        consumeToken();

                        if (currToken == Token::OpenCurlyBraces) {
                            consumeToken();
                            compoundStatement();

                            if (currToken == Token::CloseCurlyBraces) {
                                consumeToken();
                            }
                        } else {
                            statement();
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                }
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
            }
        break;
        case Token::KwDo:
            consumeToken();

            if (currToken == Token::OpenCurlyBraces) {
                consumeToken();
                compoundStatement();

                if (currToken == Token::CloseCurlyBraces) {
                    consumeToken();

                    if (currToken == Token::KwWhile) {
                        consumeToken();

                        if (currToken == Token::OpenPar) {
                            consumeToken();

                            if (isExpression(currToken)) {
                                expression();

                                if (currToken == Token::ClosePar) {
                                    consumeToken();

                                    if (currToken == Token::Semicolon) {
                                        consumeToken();
                                    } else {
                                        error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
                                    }
                                } else {
                                    error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
                                }
                            } else {
                                error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                            }
                        } else {
                            error(EXPECTED_TK, lexer.getLineNo(), "(", lexer.getLexeme());
                        }
                    } else {
                        error(EXPECTED_TK, lexer.getLineNo(), "while", lexer.getLexeme());
                    }
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "{", lexer.getLexeme());
                }
            } else {
                statement();
            }
        break;
        case Token::KwBreak:
            consumeToken();
            if (currToken == Token::Semicolon) {
                consumeToken();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
        break;
        case Token::KwContinue :
            consumeToken();
            if (currToken == Token::Semicolon) {
                consumeToken();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
        break;
        case Token::KwReturn:
            consumeToken();

            if (isExpression(currToken)) {
                expression();
            }
            if (currToken == Token::Semicolon) {
                consumeToken();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
        break;
        case Token::Include:
            headerDefinition();
        break;
        case Token::Identifier:
            consumeToken();

            if (currToken == Token::OpenPar) {
                consumeToken();
                
                while (isExpression(currToken)) {
                    expression();

                    if (currToken == Token::Comma) {
                        consumeToken();

                        if (!isExpression(currToken)) {
                            error(EXPECTED_TK, lexer.getLineNo(), "Expression", lexer.getLexeme());
                        }
                    } else {
                        break;
                    }
                }

                if (currToken == Token::ClosePar) {
                    consumeToken();
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme()); 
                }
            }

            if (currToken == Token::Semicolon) {
                consumeToken();
            } else {
                error(EXPECTED_TK, lexer.getLineNo(), ";", lexer.getLexeme());
            }
        break;
        default:
        break;
    }
}

void Parser::expression () {
    logicalTerm();
}

void Parser::logicalTerm () {
    bitwiseTerm();

    while (currToken == Token::OpAnd || currToken == Token::OpOr) {
        consumeToken();
        bitwiseTerm();
    }
}

void Parser::bitwiseTerm () {
    relationalTerm();

    while (currToken == Token::OpBitwiseOr || currToken == Token::OpBitwiseXor || currToken == Token::OpBitwiseAnd) {
        consumeToken();
        relationalTerm();
    }
}

void Parser::relationalTerm () {
    relationalGLETerm();

    while (currToken == Token::OpEqual || currToken == Token::OpNotEqual) {
        consumeToken();
        relationalGLETerm();
    }
}

void Parser::relationalGLETerm () {
    bitwiseShiftTerm();

    while (currToken == Token::OpGreater || currToken == Token::OpGreaterEqual || currToken == Token::OpLess || currToken == Token::OpLessEqual) {
        consumeToken();
        bitwiseShiftTerm();
    }
}

void Parser::bitwiseShiftTerm () {
    addSubTerm();

    while (currToken == Token::OpShiftLeft || currToken == Token::OpShiftRight) {
        consumeToken();
        addSubTerm();
    }
}

void Parser::addSubTerm () {
    mulDivModTerm();

    while (currToken == Token::OpAdd || currToken == Token::OpSub) {
        consumeToken();
        mulDivModTerm();
    }
}

void Parser::mulDivModTerm () {
    factor();

    while (currToken == Token::OpMul || currToken == Token::OpDiv || currToken == Token::OpMod) {
        consumeToken();
        factor();
    }
}

void Parser::factor() {
    if (currToken == Token::StrLiteral || currToken == Token::CharLiteral || currToken == Token::KwTrue || currToken == Token::KwFalse) {
        consumeToken();
    } else if (currToken == Token::OpenPar) {
        consumeToken();
        expression();

        if (currToken == Token::ClosePar) {
            consumeToken();
        } else {
            error(EXPECTED_TK, lexer.getLineNo(), ")", lexer.getLexeme());
        }
    } else if (isExpression(currToken)) {
        if (currToken == Token::OpLogicalNot) {
            consumeToken();
        } else if (currToken == Token::OpBitwiseNot) {
            consumeToken();
        } else if (currToken == Token::OpIncrement) {
            consumeToken();
        } else if (currToken == Token::OpDecrement) {
            consumeToken();
        }

        if (currToken == Token::Decimal || currToken == Token::Binary || currToken == Token::Hexadecimal || currToken == Token::Octal) {
            consumeToken();
        } else if (currToken == Token::Identifier) {
            consumeToken();

            if (currToken == Token::OpenBrackets) {
                consumeToken();
                expression();

                if (currToken == Token::CloseBrackets) {
                    consumeToken();
                } else {
                    error(EXPECTED_TK, lexer.getLineNo(), "]", lexer.getLexeme());
                }
            } else if (currToken == Token::OpIncrement) {
                consumeToken();
            } else if (currToken == Token::OpDecrement) {
                consumeToken();
            }
        }
    }
}