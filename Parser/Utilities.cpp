#include "Utilities.h"

void error (int errorNo, int lineNo, std::string s1, std::string s2) {
    switch (errorNo) {
        case 0 : throw "Line:" + std::to_string(lineNo) + ":Lexer: Error unknown symbol " + s2;
        case 1 : throw "Line:" + std::to_string(lineNo) + ":Lexer: Error unclosed token expected: '" + s1 + "' but found '" + s2 + "'";
        case 2 : throw "Line:" + std::to_string(lineNo) + ":Lexer: Error unknown int constant: '" + s2 + "'";
        case 3 : throw "Line:" + std::to_string(lineNo) + ":Parser: Error expected '" + s1 + "' but found '" + s2 + "'";
    }    
}

bool isStatement (Token tk) {
    switch (tk) {
        case Token::KwIf:
        case Token::KwFor:
        case Token::KwDo:
        case Token::KwBreak:
        case Token::KwContinue:
        case Token::Identifier:
        case Token::KwReturn:
        case Token::Include: return true;
        default:
            return isTypeSpecifier(tk);
    }
}

bool isExpression (Token tk) {
    switch (tk) {
        case Token::StrLiteral:
        case Token::CharLiteral:
        case Token::OpLogicalNot:
        case Token::OpBitwiseNot:
        case Token::OpIncrement:
        case Token::OpDecrement:
        case Token::Decimal:
        case Token::Hexadecimal:
        case Token::Octal:
        case Token::Binary:
        case Token::Identifier:
        case Token::OpenPar: return true;
        default:
            return false;
    }
}